package main;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Carros")
public class Carro {
	@Id
	private String placa;
	@Column(name="marca", length=200)
	private String montadora;
	private String modelo;
	private int ano;
	
	public Carro(String placa, String montadora, String modelo, int ano) {
		super();
		this.placa = placa;
		this.montadora = montadora;
		this.modelo = modelo;
		this.ano = ano;
	}
}
