package main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
	public static void main(String[] args) {
		Carro carro = new Carro("ABC-1234", "Honda", "FIT", 2018);
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Carros");
		EntityManager manager = factory.createEntityManager();
		
		manager.getTransaction().begin();
		manager.persist(carro);
		manager.getTransaction().commit();
		
		manager.close();
		factory.close();
	}
}
